const express = require('express')
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');

app.use(bodyParser.urlencoded({extended: true}))
app.use(express.json());

require('dotenv').config();

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'Eval.js'
})   

connection.connect(function (err) {
  if (err) throw err
  console.log('Connection à la BDD OKK !!')
}) // Code fonctionnel et Connecté à la BDD :)




app.set('view engine', 'pug'); //Moteur de vue avec views/index.pug

app.use(express.static(`${__dirname}/static`))

app.get('/', (req, res) => {
  connection.query(`SELECT * FROM Utilisateurs;`, (err, result, fields) => {
    if (err) {
      console.log(err)
      return
    }
    res.render('Utilisateurs/index', { results: result })
  })
})



app.get('/post/create', (req, res) => res.render('post/create'))

app.post('/post/store', (req, res) => {
  console.log(req.body.title)
  connection.query(`INSERT INTO posts (title, content, image, date) VALUES ('${req.body.title}', '${req.body.content}', '${req.body.image}', '${new Date()}');`, (err, result, fields) => {
    if (err) {
      console.log(err)
    }
  });
  res.redirect('/');
})

app.get('/post/:id', (req, res) => {
  connection.query(`SELECT * FROM posts where id = '${req.params.id}';`, (err, result, fields) => {
    if (err) {
      console.log(err);
      return
    }

    res.render('post/show', { result: result[0] })
  })
})


app.listen(process.env.PORT, () => console.log(`listen on ${process.env.PORT}`));
